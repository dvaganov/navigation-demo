package ru.gemark.common.navigation.jetpack

import android.app.Activity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import ru.gemark.common.navigation.LinkNavigator

interface JetpackRootNavigator : LinkNavigator {

    fun bindActivity(activity: Activity)
    fun bindNavController(
        tag: String,
        lifecycleOwner: LifecycleOwner,
        navController: NavController
    )
}
