package ru.gemark.common.navigation.jetpack

import android.net.Uri
import androidx.annotation.IdRes
import io.reactivex.Completable
import ru.gemark.common.navigation.LinkNavigator

interface JetpackNavigator : LinkNavigator {

    val tag: String
    val jetpackNavControllerOwner: JetpackNavControllerOwner

    fun navigateTo(@IdRes destinationId: Int, link: Uri? = null): Completable
    fun isLinkSupported(link: Uri): Boolean
    fun canPopBackStack(): Boolean
    fun backTo(@IdRes destinationId: Int): Completable
}