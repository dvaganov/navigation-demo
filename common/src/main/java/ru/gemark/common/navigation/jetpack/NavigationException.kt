package ru.gemark.common.navigation.jetpack

import android.net.Uri
import java.lang.RuntimeException

class NoBackStackException : RuntimeException("Backstack is empty")
class UnsupportedLinkException(link: Uri) : RuntimeException("Unsupported lin: $link")
