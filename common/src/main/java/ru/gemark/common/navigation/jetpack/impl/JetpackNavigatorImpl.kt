package ru.gemark.common.navigation.jetpack.impl

import android.content.Context
import android.net.Uri
import androidx.annotation.IdRes
import androidx.annotation.NavigationRes
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import io.reactivex.Completable
import ru.gemark.common.navigation.LinkNavOptions
import ru.gemark.common.navigation.jetpack.*
import ru.gemark.common.navigation.jetpack.JetpackNavControllerOwner
import java.util.*

class JetpackNavigatorImpl(
    context: Context,
    override val tag: String,
    @NavigationRes private val navGraphRes: Int,
    override val jetpackNavControllerOwner: JetpackNavControllerOwner,
    navigationBus: NavigationBus
) : JetpackNavigator,
    NavigationBus by navigationBus {

    private val backStack = LinkedList<Int>()

    private val stubNavController = NavController(context).apply {
        navigatorProvider.addNavigator(StubActivityNavigator.NAME, StubActivityNavigator())
        navigatorProvider.addNavigator(StubFragmentNavigator.NAME, StubFragmentNavigator())
        setGraph(navGraphRes)
    }

    override fun navigateTo(link: Uri, options: LinkNavOptions): Completable {
        val destinationId = getDestination(link)?.id
            ?: return Completable.error(UnsupportedLinkException(link))
        return navigateTo(destinationId, link)
    }

    override fun navigateTo(@IdRes destinationId: Int, link: Uri?): Completable {
        return jetpackNavControllerOwner.getNavController()
            .doOnSuccess { navController ->
                val currentScreen = navController.currentBackStackEntry
                val isCurrentScreenOpen = currentScreen?.lifecycle
                    ?.currentState == Lifecycle.State.RESUMED

                // добаляем только те экраны, которые были показаны пользователю
                if (
                    currentScreen != null
                    && (isCurrentScreenOpen || backStack.size > 0)
                    && navController.currentDestination?.id != destinationId
                ) {
                    backStack.push(currentScreen.destination.id)
                }

                if (navController.currentDestination?.id != destinationId) {
                    if (link != null) {
                        navController.navigate(link)
                    } else {
                        navController.navigate(destinationId)
                    }
                }
            }
            .ignoreElement()
    }

    override fun back(): Completable {
        return jetpackNavControllerOwner.getNavController()
            .doOnSuccess { navController ->
                val destinationId = backStack.poll()
                if (destinationId != null) {
                    if (navController.currentDestination?.id != destinationId) {
                        navController.popBackStack(destinationId, false)
                    }
                }
            }
            .ignoreElement()
            .onErrorComplete()
    }

    override fun isLinkSupported(link: Uri): Boolean {
        return stubNavController.graph.hasDeepLink(link)
    }

    override fun backTo(destinationId: Int): Completable {
        return jetpackNavControllerOwner.getNavController()
            .filter { it.currentDestination?.id != destinationId }
            .doOnSuccess { navController ->
                navController.popBackStack(destinationId, false)
            }
            .ignoreElement()
    }

    override fun canPopBackStack(): Boolean {
        return backStack.size > 0
    }

    private fun getDestination(link: Uri): NavDestination? {
        return stubNavController.graph.find { it.hasDeepLink(link) }
    }

    override fun toString() = tag
}
