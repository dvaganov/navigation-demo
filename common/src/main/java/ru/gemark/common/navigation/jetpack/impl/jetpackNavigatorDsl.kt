package ru.gemark.common.navigation.jetpack.impl

import android.content.Context
import androidx.annotation.IdRes
import androidx.annotation.NavigationRes
import ru.gemark.common.navigation.jetpack.JetpackCompoundNavigator
import ru.gemark.common.navigation.jetpack.JetpackNavigator
import ru.gemark.common.navigation.jetpack.NavigationBus

internal typealias JetpackNavigatorBuilderFunc = JetpackNavigatorBuilder.() -> Unit
internal typealias CompoundJetpackNavigatorBuilderFunc = CompoundJetpackNavigatorBuilder.() -> Unit

fun jetpackNavigator(context: Context, builder: JetpackNavigatorBuilderFunc): JetpackNavigator {
    return JetpackNavigatorBuilder()
        .apply(builder)
        .build(context)
}

open class JetpackNavigatorBuilder {

    @NavigationRes
    var navGraph: Int = -1
    lateinit var tag: String
    var bus: NavigationBus = NavigationBusImpl()

    open fun build(context: Context): JetpackNavigator = JetpackNavigatorImpl(
        context = context,
        tag = tag,
        navGraphRes = navGraph,
        jetpackNavControllerOwner = JetpackNavControllerOwner(),
        navigationBus = bus
    )
}

fun compoundJetpackNavigator(
    context: Context,
    builder: CompoundJetpackNavigatorBuilderFunc
): JetpackCompoundNavigator {
    return CompoundJetpackNavigatorBuilder()
        .apply(builder)
        .build(context)
}

class CompoundJetpackNavigatorBuilder : JetpackNavigatorBuilder() {

    private val children = mutableMapOf<Int, JetpackNavigator>()

    fun addChild(@IdRes destinationId: Int, navigator: JetpackNavigator) {
        children[destinationId] = navigator
    }

    override fun build(context: Context): JetpackCompoundNavigator {
        return JetpackCompoundNavigatorImpl(
            jetpackNavigator = super.build(context),
            children = children
        )
    }
}
