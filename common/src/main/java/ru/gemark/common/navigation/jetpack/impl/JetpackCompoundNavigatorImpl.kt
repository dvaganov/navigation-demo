package ru.gemark.common.navigation.jetpack.impl

import android.net.Uri
import io.reactivex.Completable
import ru.gemark.common.navigation.LinkNavOptions
import ru.gemark.common.navigation.jetpack.NoBackStackException
import ru.gemark.common.navigation.jetpack.JetpackCompoundNavigator
import ru.gemark.common.navigation.jetpack.JetpackNavigator
import ru.gemark.common.navigation.jetpack.JetpackNavControllerOwner
import java.util.*

class JetpackCompoundNavigatorImpl(
    private val jetpackNavigator: JetpackNavigator,
    private val children: Map<Int, JetpackNavigator>
) : JetpackNavigator by jetpackNavigator,
    JetpackCompoundNavigator {

    private val backStack = LinkedList<JetpackNavigator>()

    override fun getNavControllerOwner(tag: String): JetpackNavControllerOwner? {
        return if (this.tag == tag) {
            jetpackNavControllerOwner
        } else {
            for (navigator in children.values) {
                return if (navigator.tag == tag) {
                    navigator.jetpackNavControllerOwner
                } else if (navigator is JetpackCompoundNavigator) {
                    navigator.getNavControllerOwner(tag) ?: continue
                } else {
                    continue
                }
            }
            return null
        }
    }

    override fun navigateTo(link: Uri, options: LinkNavOptions): Completable {
        return if (jetpackNavigator.isLinkSupported(link)) {
            jetpackNavigator.navigateTo(link, options)
                .doOnComplete { backStack.push(jetpackNavigator) }
        } else {
            children.entries
                .find { (_, navigator) -> navigator.isLinkSupported(link) }
                ?.let { (destinationId, navigator) ->
                    navigateTo(destinationId)
                        .doOnComplete { backStack.push(navigator) }
                        .andThen(navigator.navigateTo(link))
                }
                ?: Completable.complete()
        }
    }

    override fun isLinkSupported(link: Uri): Boolean {
        return jetpackNavigator.isLinkSupported(link)
                || children.values.any { it.isLinkSupported(link) }
    }

    override fun back(): Completable {
        val topNavigator = backStack.poll()
            ?: return Completable.error(NoBackStackException())
        val nextNavigator = backStack.peek() ?: jetpackNavigator

        return when {
            topNavigator == jetpackNavigator && jetpackNavigator.canPopBackStack() -> {
                jetpackNavigator.back()
            }
            nextNavigator == jetpackNavigator && jetpackNavigator.canPopBackStack() -> {
                jetpackNavigator.back()
                    .andThen(topNavigator.back())
            }
            topNavigator.canPopBackStack() -> {
                topNavigator.back()
            }
            topNavigator == nextNavigator -> {
                back()
            }
            else -> Completable.error(NoBackStackException())
        }
    }

    override fun canPopBackStack(): Boolean {
        return jetpackNavigator.canPopBackStack()
                || backStack.size > 1
                || children.values.any { it.canPopBackStack() }
    }

    override fun toString(): String {
        return "*$tag"
    }
}
