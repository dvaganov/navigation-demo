package ru.gemark.common.navigation.jetpack

interface JetpackCompoundNavigator : JetpackNavigator {
    fun getNavControllerOwner(tag: String): JetpackNavControllerOwner?
}
