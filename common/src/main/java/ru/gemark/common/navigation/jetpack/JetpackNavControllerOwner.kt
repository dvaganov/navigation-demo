package ru.gemark.common.navigation.jetpack

import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import io.reactivex.Maybe

interface JetpackNavControllerOwner {

    fun getNavController(): Maybe<NavController>

    fun bind(
        lifecycleOwner: LifecycleOwner,
        navController: NavController
    )
}
