package ru.gemark.common.navigation

import android.net.Uri
import io.reactivex.Completable
import ru.gemark.common.navigation.jetpack.NavigationBus

interface LinkNavigator : NavigationBus {
    fun navigateTo(link: Uri, options: LinkNavOptions = LinkNavOptions()): Completable
    fun back(): Completable
}
