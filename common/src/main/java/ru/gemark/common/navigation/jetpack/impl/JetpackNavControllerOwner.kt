package ru.gemark.common.navigation.jetpack.impl

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.NavController
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Maybe
import ru.gemark.common.navigation.jetpack.JetpackNavControllerOwner

class JetpackNavControllerOwner : JetpackNavControllerOwner {

    private val navControllerRelay = BehaviorRelay.create<Any>()
    private val boundNavControllersHash = mutableSetOf<Int>()

    override fun bind(lifecycleOwner: LifecycleOwner, navController: NavController) {
        val id = lifecycleOwner.hashCode()
        if (boundNavControllersHash.contains(id)) return

        boundNavControllersHash.add(id)
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun onStart() {
                navControllerRelay.accept(navController)
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onStop() {
                navControllerRelay.accept(Unit)
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                boundNavControllersHash.remove(lifecycleOwner.hashCode())
            }
        })
    }

    override fun getNavController(): Maybe<NavController> {
        return navControllerRelay.hide()
            .filter { it is NavController }
            .cast(NavController::class.java)
            .firstElement()
    }
}
