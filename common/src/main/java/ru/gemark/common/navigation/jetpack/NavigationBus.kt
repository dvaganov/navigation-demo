package ru.gemark.common.navigation.jetpack

import io.reactivex.Observable

interface NavigationBus {
    fun <T> receiveResult(key: String, clazz: Class<T>): Observable<T>
    fun sendResult(key: String, value: Any)
}