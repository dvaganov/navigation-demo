package ru.gemark.common.navigation.jetpack.impl

import android.os.Bundle
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator

class StubFragmentNavigator : Navigator<NavDestination>() {

    override fun createDestination(): NavDestination {
        return NavDestination(NAME)
    }

    override fun navigate(
        destination: NavDestination,
        args: Bundle?,
        navOptions: NavOptions?,
        navigatorExtras: Extras?
    ): NavDestination? {
        return createDestination()
    }

    override fun popBackStack(): Boolean {
        return false
    }

    companion object {
        const val NAME = "fragment"
    }
}
