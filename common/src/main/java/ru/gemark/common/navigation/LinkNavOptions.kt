package ru.gemark.common.navigation

data class LinkNavOptions(
    val skipBackStack: Boolean = false
)