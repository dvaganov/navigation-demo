package ru.gemark.common.navigation.jetpack.impl

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import ru.gemark.common.navigation.jetpack.NavigationBus

class NavigationBusImpl : NavigationBus {

    private val bus = PublishRelay.create<BusEvent>()

    override fun sendResult(key: String, value: Any) {
        bus.accept(BusEvent(key, value))
    }

    override fun <T> receiveResult(key: String, clazz: Class<T>): Observable<T> {
        return bus.hide()
            .filter { it.key == key && clazz.isInstance(it.value) }
            .map { clazz.cast(it.value) }
    }

    private data class BusEvent(
        val key: String,
        val value: Any
    )
}
