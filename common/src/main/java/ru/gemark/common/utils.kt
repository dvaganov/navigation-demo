package ru.gemark.common

import android.util.Log

fun log(obj: Any, data: String) {
    Log.d("!!!!!", "$data -- (${obj::class.simpleName}:${obj.hashCode()})")
}