package ru.gemark.common.di

object CommonDelegate {

    private lateinit var navigationApi: NavigationApi
    private var component: CommonComponent? = null

    fun setDependencies(navigationApi: NavigationApi) {
        this.navigationApi = navigationApi
    }

    fun getComponent(): CommonComponent {
        return component ?: DaggerCommonComponent.factory()
            .create(navigationApi)
            .also { component = it }
    }
}
