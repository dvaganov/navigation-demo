package ru.gemark.common.di

import dagger.Component
import ru.gemark.common.presentation.NavActivity
import ru.gemark.common.presentation.NavFragment
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [
        NavigationApi::class
    ]
)
interface CommonComponent {

    @Component.Factory
    interface Factory {
        fun create(
            navigationApi: NavigationApi
        ): CommonComponent
    }

    fun inject(act: NavActivity)
    fun inject(frag: NavFragment)
}
