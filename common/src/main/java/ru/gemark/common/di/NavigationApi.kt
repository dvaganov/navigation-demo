package ru.gemark.common.di

import ru.gemark.common.navigation.jetpack.JetpackRootNavigator
import ru.gemark.common.navigation.LinkNavigator

interface NavigationApi {
    fun provideAppNavigator(): LinkNavigator
    fun provideRootNavigator(): JetpackRootNavigator
}