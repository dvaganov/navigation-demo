package ru.gemark.common

import androidx.lifecycle.*
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

fun <T> LiveData<T>.asObservable(): Observable<T> {
    val rxLifecycleObserver = RxLifecycleObserver<T>()
    observe(rxLifecycleObserver, rxLifecycleObserver)
    return rxLifecycleObserver.observable
}

class RxLifecycleObserver<T> : LifecycleOwner, Observer<T> {

    val observable: Observable<T>
        get() = subject.hide()
            .doOnSubscribe {
                lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
            }
            .doOnDispose {
                lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            }
            .share()

    private val subject = BehaviorSubject.create<T>()
    private val lifecycle = LifecycleRegistry(this)

    init {
        lifecycle.currentState = Lifecycle.State.CREATED
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycle
    }

    override fun onChanged(t: T) {
        subject.onNext(t)
    }

}