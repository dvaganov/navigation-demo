package ru.gemark.common.presentation

import android.os.Bundle
import androidx.activity.addCallback
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import ru.gemark.common.di.CommonDelegate
import ru.gemark.common.navigation.jetpack.JetpackRootNavigator
import java.lang.RuntimeException
import javax.inject.Inject

abstract class NavFragment(@LayoutRes layoutRes: Int) : Fragment(layoutRes) {

    @Inject
    internal lateinit var rootNavigator: JetpackRootNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CommonDelegate.getComponent().inject(this)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            rootNavigator.back().subscribe()
        }
    }

    protected fun initNavigator(navTag: String, @IdRes containerId: Int) {
        rootNavigator.bindNavController(
            tag = navTag,
            lifecycleOwner = this,
            navController = requiredNavController(containerId)
        )
    }

    private fun requiredNavController(@IdRes hostId: Int): NavController {
        return (childFragmentManager.findFragmentById(hostId) as? NavHostFragment)?.navController
            ?: throw RuntimeException("NavController does't exists")
    }
}
