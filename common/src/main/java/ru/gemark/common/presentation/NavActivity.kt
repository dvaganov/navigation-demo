package ru.gemark.common.presentation

import android.os.Bundle
import androidx.activity.addCallback
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import ru.gemark.common.di.CommonDelegate
import ru.gemark.common.navigation.jetpack.JetpackRootNavigator
import java.lang.RuntimeException
import javax.inject.Inject

abstract class NavActivity(
    @LayoutRes private val layoutRes: Int
) : FragmentActivity() {

    @Inject
    //todo: make internal
    lateinit var rootNavigator: JetpackRootNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        CommonDelegate.getComponent().inject(this)

        onBackPressedDispatcher.addCallback(this) {
            rootNavigator.back()
                .subscribe()
        }
    }

    protected fun initNavigator(navTag: String, @IdRes containerId: Int) {
        rootNavigator.bindNavController(
            tag = navTag,
            lifecycleOwner = this,
            navController = requiredNavController(containerId)
        )
    }

    override fun onStart() {
        super.onStart()
        rootNavigator.bindActivity(this)
    }

    private fun requiredNavController(@IdRes hostId: Int): NavController {
        return (supportFragmentManager.findFragmentById(hostId) as? NavHostFragment)?.navController
            ?: throw RuntimeException("NavController does't exists")
    }
}
