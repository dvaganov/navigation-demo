package ru.gemark.ott.di

import dagger.Component
import ru.gemark.common.di.NavigationApi
import ru.gemark.ott.presentation.OttActivity
import ru.gemark.ott.presentation.frag.BookFragment
import ru.gemark.ott.presentation.frag.CategoriesFragment
import ru.gemark.ott.presentation.frag.OttScreenFragment
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [
        NavigationApi::class
    ],
    modules = [

    ]
)
interface OttComponent {

    @Component.Factory
    interface Factory {
        fun create(
            navigationApi: NavigationApi
        ): OttComponent
    }

    fun inject(activity: OttActivity)
    fun inject(frag: OttScreenFragment)
    fun inject(frag: CategoriesFragment)
    fun inject(frag: BookFragment)
}