package ru.gemark.ott.di

import ru.gemark.common.di.NavigationApi

object OttDelegate {

    private var component: OttComponent? = null
    private lateinit var navigationApi: NavigationApi

    fun setDependencies(navigationApi: NavigationApi) {
        this.navigationApi = navigationApi
    }

    fun getComponent(): OttComponent {
        return component ?: DaggerOttComponent.factory()
            .create(
                navigationApi = navigationApi
            )
            .also { component = it }
    }
}
