package ru.gemark.ott.presentation.frag

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.frag_book.*
import ru.gemark.common.navigation.jetpack.JetpackRootNavigator
import ru.gemark.common.presentation.NavFragment
import ru.gemark.ott.R
import ru.gemark.ott.di.OttDelegate
import ru.gemark.ott.presentation.navigation.OTT_BOOK_NAV_TAG
import javax.inject.Inject

class BookFragment : NavFragment(R.layout.frag_book) {

    private val args: BookFragmentArgs by navArgs()

    @Inject
    internal lateinit var nestedRootNavigator: JetpackRootNavigator

    init {
        OttDelegate.getComponent().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigator(OTT_BOOK_NAV_TAG, R.id.book_host)

        button.setOnClickListener {
            nestedRootNavigator.sendResult("ott", "Hello from ott! ${args.data}")
        }
    }
}
