package ru.gemark.ott.presentation.frag

import android.os.Bundle
import android.view.View
import ru.gemark.common.presentation.NavFragment
import ru.gemark.ott.R
import ru.gemark.ott.presentation.navigation.OTT_CATEGORIES_NAV_TAG

class CategoriesFragment : NavFragment(R.layout.frag_categories) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigator(OTT_CATEGORIES_NAV_TAG, R.id.offer_host)
    }
}