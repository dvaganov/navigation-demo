package ru.gemark.ott.presentation

import android.os.Bundle
import androidx.navigation.navArgs
import ru.gemark.common.presentation.NavActivity
import ru.gemark.ott.R
import ru.gemark.ott.presentation.navigation.OTT_NAV_TAG

class OttActivity : NavActivity(R.layout.activity_ott) {

    private val args: OttActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNavigator(OTT_NAV_TAG, R.id.main_fragment_container)
    }
}
