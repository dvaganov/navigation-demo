package ru.gemark.ott.presentation.frag

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.frag_ott_screen.*
import ru.gemark.common.navigation.LinkNavigator
import ru.gemark.ott.R
import ru.gemark.ott.di.OttDelegate
import ru.gemark.ott.presentation.BaseOttFragment
import javax.inject.Inject

class OttScreenFragment : BaseOttFragment(R.layout.frag_ott_screen) {

    @Inject
    internal lateinit var appNavigator: LinkNavigator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setText("Hello ott screen! Let's do some <a href=\"yota://self.care/offer/book\">booking</a>!")
        OttDelegate.getComponent().inject(this)

        button.setOnClickListener {
//            appNavigator.openMain()
//            val testDto = OttDto(10)
//            val navController = findNavController()
//            val uri = Uri.parse("yota-ott://self.care/offer/book?data=${testDto}")
//
//            if (navController.graph.hasDeepLink(uri)) {
//                navController.navigate(uri)
//            }
        }
    }
}