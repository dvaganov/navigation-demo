package ru.gemark.ott.presentation.navigation

import android.content.Context
import ru.gemark.common.navigation.jetpack.JetpackNavigator
import ru.gemark.common.navigation.jetpack.NavigationBus
import ru.gemark.common.navigation.jetpack.impl.compoundJetpackNavigator
import ru.gemark.common.navigation.jetpack.impl.jetpackNavigator
import ru.gemark.ott.R

const val OTT_NAV_TAG = "OttNav"
const val OTT_CATEGORIES_NAV_TAG = "CategoriesNav"
const val OTT_BOOK_NAV_TAG = "BookNav"

fun createOttNavigator(context: Context, navigationBus: NavigationBus): JetpackNavigator {
    return compoundJetpackNavigator(context) {
        tag = OTT_NAV_TAG
        navGraph = R.navigation.ott_graph
        bus = navigationBus

        addChild(R.id.categoriesFragment, compoundJetpackNavigator(context) {
            tag = OTT_CATEGORIES_NAV_TAG
            navGraph = R.navigation.ott_offer_graph
            bus = navigationBus

            addChild(R.id.bookFragment, jetpackNavigator(context) {
                tag = OTT_BOOK_NAV_TAG
                navGraph = R.navigation.ott_book_graph
                bus = navigationBus
            })
        })
    }
}
