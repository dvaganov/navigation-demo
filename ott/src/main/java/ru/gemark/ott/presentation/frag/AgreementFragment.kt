package ru.gemark.ott.presentation.frag

import android.os.Bundle
import android.view.View
import ru.gemark.ott.presentation.BaseOttFragment

class AgreementFragment : BaseOttFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setText("Agreement")
    }
}