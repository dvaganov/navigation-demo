package ru.gemark.ott.presentation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OttDto(
    val offerId: Int
) : Parcelable