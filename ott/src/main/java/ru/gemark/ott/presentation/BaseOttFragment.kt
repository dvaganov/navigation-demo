package ru.gemark.ott.presentation

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.text.parseAsHtml
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_ott.*
import ru.gemark.ott.R

abstract class BaseOttFragment(
    private val layoutId: Int? = null
) : Fragment(R.layout.frag_ott) {

    protected fun setText(text: String) {
        ott_tv.text = text.parseAsHtml()
        ott_tv.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        if (layoutId != null) {
            inflater.inflate(
                layoutId,
                view?.findViewById<FrameLayout>(R.id.container),
                true
            )
        }
        return view
    }

}