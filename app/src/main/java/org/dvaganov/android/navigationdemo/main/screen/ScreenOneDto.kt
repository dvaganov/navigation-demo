package org.dvaganov.android.navigationdemo.main.screen

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScreenOneDto(
    val value: String
) : Parcelable