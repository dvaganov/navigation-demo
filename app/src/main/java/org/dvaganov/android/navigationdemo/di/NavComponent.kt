package org.dvaganov.android.navigationdemo.di

import dagger.BindsInstance
import dagger.Component
import org.dvaganov.android.navigationdemo.Application
import ru.gemark.common.di.NavigationApi
import javax.inject.Singleton

@Component(
    modules = [
        NavModule::class
    ]
)
@Singleton
internal interface NavComponent : NavigationApi {

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance application: Application
        ): NavComponent
    }
}
