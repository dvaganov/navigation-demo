package org.dvaganov.android.navigationdemo

import android.app.Activity
import android.net.Uri
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.navigation.ActivityNavigator
import androidx.navigation.NavController
import io.reactivex.Completable
import org.dvaganov.android.navigationdemo.main.navigation.createMainNavigator
import ru.gemark.common.navigation.LinkNavOptions
import ru.gemark.common.navigation.jetpack.JetpackCompoundNavigator
import ru.gemark.common.navigation.jetpack.JetpackRootNavigator
import ru.gemark.common.navigation.jetpack.NoBackStackException
import ru.gemark.common.navigation.jetpack.impl.compoundJetpackNavigator
import ru.gemark.ott.presentation.navigation.createOttNavigator
import java.lang.ref.WeakReference
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppNavigatorImpl @Inject constructor(
    application: Application
) : JetpackRootNavigator {

    private var currentActivity: WeakReference<Activity>? = null

    private val jetpackCompoundNavigator: JetpackCompoundNavigator =
        compoundJetpackNavigator(application) {
            tag = "AppNav"
            navGraph = R.navigation.app_graph
            addChild(R.id.mainActivity, createMainNavigator(application, bus))
            addChild(R.id.ott_activity_graph, createOttNavigator(application, bus))
        }

    init {
        jetpackCompoundNavigator.jetpackNavControllerOwner.bind(
            navController = NavController(application)
                .apply { setGraph(R.navigation.app_graph) },
            lifecycleOwner = ProcessLifecycleOwner.get()
        )
    }

    override fun bindActivity(activity: Activity) {
        currentActivity = WeakReference(activity)

        jetpackCompoundNavigator.jetpackNavControllerOwner
            .getNavController()
            .doOnSuccess {
                it.navigatorProvider.addNavigator("activity", ActivityNavigator(activity))
            }
            .subscribe()
    }

    override fun bindNavController(
        tag: String,
        lifecycleOwner: LifecycleOwner,
        navController: NavController
    ) {
        jetpackCompoundNavigator.getNavControllerOwner(tag)
            ?.bind(lifecycleOwner, navController)
    }

    override fun navigateTo(link: Uri, options: LinkNavOptions) =
        jetpackCompoundNavigator.navigateTo(link, options)

    override fun back(): Completable {
        return jetpackCompoundNavigator.back()
            .onErrorResumeNext {
                if (it is NoBackStackException) {
                    currentActivity?.get()?.finish()
                    Completable.complete()
                } else {
                    Completable.error(it)
                }
            }
    }

    override fun sendResult(key: String, value: Any) =
        jetpackCompoundNavigator.sendResult(key, value)

    override fun <T> receiveResult(key: String, clazz: Class<T>) =
        jetpackCompoundNavigator.receiveResult(key, clazz)
}
