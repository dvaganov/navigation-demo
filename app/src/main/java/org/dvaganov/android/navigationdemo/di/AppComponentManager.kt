package org.dvaganov.android.navigationdemo.di

import org.dvaganov.android.navigationdemo.Application
import ru.gemark.common.di.NavigationApi

object AppComponentManager {

    private var navComponent: NavComponent? = null

    private lateinit var application: Application

    fun setDependencies(
        application: Application
    ) {
        this.application = application
    }

    fun getNavigationApi(): NavigationApi {
        return navComponent ?: DaggerNavComponent.factory()
            .create(
                application = application
            )
            .also { navComponent = it }
    }
}