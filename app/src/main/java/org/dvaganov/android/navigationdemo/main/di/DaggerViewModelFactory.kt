package org.dvaganov.android.navigationdemo.main.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.dvaganov.android.navigationdemo.main.MainViewModel
import org.dvaganov.android.navigationdemo.main.screen.ScreenOneViewModel
import javax.inject.Inject
import javax.inject.Provider

internal class DaggerViewModelFactory @Inject constructor(
    private val mainVmProvider: Provider<MainViewModel>,
    private val screenOneVmProvider: Provider<ScreenOneViewModel>
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            MainViewModel::class.java -> mainVmProvider.get() as? T
            ScreenOneViewModel::class.java -> screenOneVmProvider.get() as? T
            else -> null
        } ?: throw Exception("Unknown view model")
    }

}