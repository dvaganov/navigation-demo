package org.dvaganov.android.navigationdemo.main.di

import dagger.Component
import org.dvaganov.android.navigationdemo.main.MainActivity
import org.dvaganov.android.navigationdemo.core.presentation.VmProvider
import org.dvaganov.android.navigationdemo.main.MainFragment
import org.dvaganov.android.navigationdemo.main.screen.DefaultScreenFragment
import org.dvaganov.android.navigationdemo.main.screen.ScreenOneFragment
import org.dvaganov.android.navigationdemo.main.screen.ScreenTwoFragment
import ru.gemark.common.di.NavigationApi
import javax.inject.Singleton

@Component(
    dependencies = [NavigationApi::class],
    modules = []
)
@Singleton
interface MainComponent {

    @Component.Factory
    interface Factory {
        fun create(
            navApi: NavigationApi
        ): MainComponent
    }

    fun inject(vmProvider: VmProvider)
    fun inject(frag: MainFragment)
    fun inject(frag: DefaultScreenFragment)
    fun inject(frag: ScreenOneFragment)
    fun inject(frag: ScreenTwoFragment)

    fun inject(act: MainActivity)
}
