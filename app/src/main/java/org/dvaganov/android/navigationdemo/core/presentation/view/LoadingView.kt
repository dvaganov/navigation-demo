package org.dvaganov.android.navigationdemo.core.presentation.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.AttrRes
import io.reactivex.Completable
import kotlinx.android.synthetic.main.layout_loading.view.*
import org.dvaganov.android.navigationdemo.R

class LoadingView @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    @AttrRes defStyleAttr: Int = 0
) : FrameLayout(ctx, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(ctx)
            .inflate(R.layout.layout_loading, this)

        setBackgroundColor(Color.WHITE)
    }

    fun startAnimation() {
        progressBar?.alpha = 1f
    }

    fun stopAnimation(): Completable {
        return Completable.create { emitter ->
            when {
                progressBar == null -> emitter.onComplete()
                progressBar.alpha > 0 -> {
                    progressBar.animate()
                        .alpha(0f)
                        .withEndAction { emitter.onComplete() }
                        .start()
                }
                else -> emitter.onComplete()
            }
        }
    }

}