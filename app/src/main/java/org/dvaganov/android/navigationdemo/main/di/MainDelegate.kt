package org.dvaganov.android.navigationdemo.main.di

import ru.gemark.common.di.NavigationApi

object MainDelegate {

    private var mainComponent: MainComponent? = null
    private lateinit var navigationApi: NavigationApi

    fun setDependencies(navigationApi: NavigationApi) {
        this.navigationApi = navigationApi
    }

    fun getMainComponent(): MainComponent {
        return mainComponent ?: DaggerMainComponent.factory()
            .create(
                navApi = navigationApi
            )
            .also { mainComponent = it }
    }
}
