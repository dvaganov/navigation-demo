package org.dvaganov.android.navigationdemo

import android.app.Application
import org.dvaganov.android.navigationdemo.di.AppComponentManager
import org.dvaganov.android.navigationdemo.main.di.MainDelegate
import ru.gemark.common.di.CommonDelegate
import ru.gemark.ott.di.OttDelegate

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        AppComponentManager.setDependencies(
            application = this
        )

        CommonDelegate.setDependencies(
            navigationApi = AppComponentManager.getNavigationApi()
        )
        MainDelegate.setDependencies(
            navigationApi = AppComponentManager.getNavigationApi()
        )
        OttDelegate.setDependencies(
            navigationApi = AppComponentManager.getNavigationApi()
        )
    }
}