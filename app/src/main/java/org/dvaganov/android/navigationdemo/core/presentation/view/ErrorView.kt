package org.dvaganov.android.navigationdemo.core.presentation.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.AttrRes
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.view_error.view.*
import org.dvaganov.android.navigationdemo.R

class ErrorView @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    @AttrRes defStyleAttr: Int = 0
) : FrameLayout(ctx, attrs, defStyleAttr) {

    val mainButtonClicks = PublishSubject.create<Unit>()

    init {
        LayoutInflater.from(ctx)
            .inflate(R.layout.view_error, this)
        button.setOnClickListener { mainButtonClicks.onNext(Unit) }
    }

}