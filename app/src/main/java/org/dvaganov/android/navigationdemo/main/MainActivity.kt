package org.dvaganov.android.navigationdemo.main

import android.net.Uri
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_module.*
import org.dvaganov.android.navigationdemo.R
import org.dvaganov.android.navigationdemo.main.navigation.MAIN_NAV_TAG
import ru.gemark.common.presentation.NavActivity

class MainActivity : NavActivity(R.layout.activity_module) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNavigator(MAIN_NAV_TAG, R.id.main_fragment_container)

        btn_back.setOnClickListener {
            rootNavigator.back()
                .subscribe()
        }

        btn_main.setOnClickListener {
            rootNavigator.navigateTo(Uri.parse("yota-main://screen-two"))
                .subscribe()
        }

        btn_other.setOnClickListener {
            rootNavigator.navigateTo(Uri.parse("yota-main://other"))
                .subscribe()
        }
    }
}
