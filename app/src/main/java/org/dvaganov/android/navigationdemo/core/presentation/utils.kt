package org.dvaganov.android.navigationdemo.core.presentation

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import org.dvaganov.android.navigationdemo.main.di.DaggerViewModelFactory
import javax.inject.Inject

inline fun <reified T : ViewModel> Fragment.setupVm() {
//    val vmProvider = VmProvider<T>()
//    ComponentManager.getMainComponent().inject(vmProvider)

}

class VmProvider(
    private val fragment: Fragment
) {

    @Inject
    internal lateinit var factory: DaggerViewModelFactory

//    fun <T : ViewModel> createVm(clazz: Class<T>) {
//        fragment.viewModels<T> { factory }
//    }

}