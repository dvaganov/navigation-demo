package org.dvaganov.android.navigationdemo.di

import dagger.Module
import dagger.Provides
import org.dvaganov.android.navigationdemo.AppNavigatorImpl
import ru.gemark.common.navigation.jetpack.JetpackRootNavigator
import ru.gemark.common.navigation.LinkNavigator
import javax.inject.Singleton

@Module
class NavModule {

    @Provides
    @Singleton
    fun provideAppNavigator(navigator: AppNavigatorImpl): LinkNavigator = navigator

    @Provides
    @Singleton
    fun provideRootNavigator(navigator: AppNavigatorImpl): JetpackRootNavigator = navigator

}