package org.dvaganov.android.navigationdemo.main.screen

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import kotlinx.android.synthetic.main.frag_screen_default.*
import org.dvaganov.android.navigationdemo.R
import org.dvaganov.android.navigationdemo.main.MainViewModel
import org.dvaganov.android.navigationdemo.main.di.MainDelegate
import org.dvaganov.android.navigationdemo.main.di.DaggerViewModelFactory
import javax.inject.Inject

class DefaultScreenFragment : Fragment(R.layout.frag_screen_default) {

    @Inject
    internal lateinit var factory: DaggerViewModelFactory
    private val vm: MainViewModel by viewModels { factory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        MainDelegate.getMainComponent().inject(this)

        btn_one.setOnClickListener { vm.execute(MainViewModel.Action.ONE) }
        btn_two.setOnClickListener { vm.execute(MainViewModel.Action.TWO) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().moveTaskToBack(true)
                }
            }
        )
    }

}