package org.dvaganov.android.navigationdemo.main.screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.frag_screen_one.*
import org.dvaganov.android.navigationdemo.R
import org.dvaganov.android.navigationdemo.core.presentation.view.ErrorView
import org.dvaganov.android.navigationdemo.core.presentation.view.LoadingView
import org.dvaganov.android.navigationdemo.core.presentation.view.StateLayout
import org.dvaganov.android.navigationdemo.main.di.MainDelegate
import org.dvaganov.android.navigationdemo.main.di.DaggerViewModelFactory
import javax.inject.Inject

class ScreenOneFragment : Fragment(R.layout.frag_screen_one) {

    @Inject
    internal lateinit var factory: DaggerViewModelFactory

    private lateinit var errorView: ErrorView
    private lateinit var loadingView: LoadingView

    private val vm: ScreenOneViewModel by viewModels { factory }
    private val rxBinds = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainDelegate.getMainComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState) ?: return null
        val stateLayout = view.findViewById<StateLayout>(R.id.state_layout)

        loadingView = LoadingView(view.context)
        errorView = ErrorView(view.context)

        stateLayout?.addState(LOADING_STATE, loadingView)
        stateLayout?.addState(ERROR_STATE, errorView)

        stateLayout?.state = vm.state.value?.mapToState() ?: StateLayout.DEFAULT_STATE

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.createBinds()
        createBinds()
    }

    private fun createBinds() {
        rxBinds.addAll(
            vm.state
                .observeOn(AndroidSchedulers.mainThread())
                .flatMapCompletable {
                    loadingView.stopAnimation()
                        .doOnComplete {
                            if (it is ScreenOneViewModel.ScreenOneState.Loading) {
                                loadingView.startAnimation()
                            }
                            state_layout?.state = it.mapToState()
                        }
                }
                .subscribe(),
            errorView.mainButtonClicks
                .subscribe { vm.errorMainAction.onNext(Unit) }
        )
    }

    private fun ScreenOneViewModel.ScreenOneState.mapToState(): String {
        return when (this) {
            is ScreenOneViewModel.ScreenOneState.Loading -> LOADING_STATE
            is ScreenOneViewModel.ScreenOneState.Error -> ERROR_STATE
            is ScreenOneViewModel.ScreenOneState.Default -> StateLayout.DEFAULT_STATE
        }
    }

    companion object {
        const val LOADING_STATE = "LOADING_STATE"
        const val ERROR_STATE = "ERROR_STATE"
    }

}