package org.dvaganov.android.navigationdemo.main.screen

import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.lang.RuntimeException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ScreenOneViewModel @Inject constructor() : ViewModel() {

    val state = BehaviorSubject.createDefault<ScreenOneState>(ScreenOneState.Loading)
    val errorMainAction = PublishSubject.create<Unit>()

    private val rxBinds = CompositeDisposable()

    init {
        remoteRequest(withError = true)
            .doOnSubscribe { state.onNext(ScreenOneState.Loading) }
            .doOnComplete { state.onNext(ScreenOneState.Default) }
            .doOnError { state.onNext(ScreenOneState.Error("")) }
            .subscribeBy(
                onError = {},
                onComplete = {}
            ).addTo(rxBinds)
    }

    fun createBinds() {
        rxBinds.addAll(
            errorMainAction
                .flatMapCompletable {
                    remoteRequest(withError = false)
                        .doOnSubscribe { state.onNext(ScreenOneState.Loading) }
                        .doOnError { state.onNext(ScreenOneState.Error("")) }
                        .doOnComplete { state.onNext(ScreenOneState.Default) }
                }
                .subscribeBy()
        )
    }

    private fun remoteRequest(withError: Boolean): Completable {
        return Completable.complete()
            .delay(2000L, TimeUnit.MILLISECONDS)
            .observeOn(Schedulers.io())
            .andThen(
                if (withError) {
                    Completable.error(RuntimeException())
                } else {
                    Completable.complete()
                }
            )
    }

    override fun onCleared() {
        super.onCleared()
        rxBinds.clear()
    }

    sealed class ScreenOneState {
        object Default : ScreenOneState()
        object Loading : ScreenOneState()
        data class Error(val data: Any) : ScreenOneState()
    }

}