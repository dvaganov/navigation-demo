package org.dvaganov.android.navigationdemo.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import kotlinx.android.synthetic.main.frag_main.*
import org.dvaganov.android.navigationdemo.R
import org.dvaganov.android.navigationdemo.main.di.MainDelegate
import org.dvaganov.android.navigationdemo.main.di.DaggerViewModelFactory
import org.dvaganov.android.navigationdemo.main.navigation.MAIN_HOME_NAV_TAG
import ru.gemark.common.presentation.NavFragment
import javax.inject.Inject

class MainFragment : NavFragment(R.layout.frag_main) {

    @Inject
    internal lateinit var factory: DaggerViewModelFactory

    private val vm: MainViewModel by viewModels { factory }

    init {
        MainDelegate.getMainComponent().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_back.setOnClickListener { vm.openOtt() }
        initNavigator(MAIN_HOME_NAV_TAG, R.id.nested_container)
    }
}
