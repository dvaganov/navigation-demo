package org.dvaganov.android.navigationdemo.main.navigation

import android.content.Context
import org.dvaganov.android.navigationdemo.R
import ru.gemark.common.navigation.jetpack.JetpackNavigator
import ru.gemark.common.navigation.jetpack.NavigationBus
import ru.gemark.common.navigation.jetpack.impl.compoundJetpackNavigator
import ru.gemark.common.navigation.jetpack.impl.jetpackNavigator

const val MAIN_NAV_TAG = "MainNav"
const val MAIN_HOME_NAV_TAG = "MainHomeNav"

fun createMainNavigator(context: Context, navigationBus: NavigationBus): JetpackNavigator {
    return compoundJetpackNavigator(context) {
        tag = MAIN_NAV_TAG
        navGraph = R.navigation.module_graph
        bus = navigationBus

        addChild(R.id.mainFragment, jetpackNavigator(context) {
            tag = MAIN_HOME_NAV_TAG
            navGraph = R.navigation.main_graph
            bus = navigationBus
        })
    }
}
