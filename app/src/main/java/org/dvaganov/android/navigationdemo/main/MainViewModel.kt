package org.dvaganov.android.navigationdemo.main

import android.net.Uri
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import ru.gemark.common.navigation.LinkNavigator
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class MainViewModel @Inject constructor(
    private val appNavigator: LinkNavigator
) : ViewModel() {

    fun openOtt() {
        appNavigator.navigateTo(Uri.parse("yota-ott://book?data=\"some\""))
            .subscribe()
    }

    fun getResult(key: String): Observable<String> {
        return appNavigator.receiveResult(key, String::class.java)
    }

    fun openOther() {
    }

    fun execute(action: Action) {
        when (action) {
            Action.TWO -> {
                appNavigator.navigateTo(Uri.parse("yota-main://screen-two"))
                    .subscribe()
            }
            else -> Unit
        }
    }

    enum class Action {
        BACK, ONE, TWO
    }

}