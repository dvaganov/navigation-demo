package org.dvaganov.android.navigationdemo.core.presentation.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.AttrRes
import androidx.core.view.children
import java.lang.RuntimeException

class StateLayout @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    @AttrRes defStyleAttr: Int = 0
) : FrameLayout(ctx, attrs, defStyleAttr) {

    var state: String = ""
        set(value) {
            field = value
            changeState(value)
        }

    private val stateMap = mutableMapOf<String, View>()

    fun addState(tag: String, view: View) {
        stateMap[tag] = view
    }

    private fun changeState(tag: String) {
        if (tag == DEFAULT_STATE) {
            resetView()
            return
        }
        val view = stateMap[tag] ?: throw RuntimeException("No state $tag")
        replaceView(view)
    }

    private fun resetView() {
        children.forEach {
            if (stateMap.values.contains(it)) {
                removeView(it)
            } else {
                it.animate()
                    .withStartAction { it.visibility = View.VISIBLE }
                    .alpha(1f)
                    .start()
            }
        }
    }

    private fun replaceView(view: View) {
        children.forEach {
            if (stateMap.values.contains(it)) {
                removeView(it)
            } else {
                it.visibility = GONE
                it.alpha = 0f
            }
        }
        addView(
            view,
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        )
    }

    companion object {
        const val DEFAULT_STATE = "DEFAULT_STATE"
    }

}
