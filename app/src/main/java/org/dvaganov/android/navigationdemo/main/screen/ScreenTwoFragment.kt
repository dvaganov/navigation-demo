package org.dvaganov.android.navigationdemo.main.screen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.frag_screen_two.*
import org.dvaganov.android.navigationdemo.R
import org.dvaganov.android.navigationdemo.main.MainViewModel
import org.dvaganov.android.navigationdemo.main.di.DaggerViewModelFactory
import org.dvaganov.android.navigationdemo.main.di.MainDelegate
import javax.inject.Inject

class ScreenTwoFragment : Fragment(R.layout.frag_screen_two) {

    @Inject
    internal lateinit var factory: DaggerViewModelFactory

    private val vm: MainViewModel by viewModels { factory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        MainDelegate.getMainComponent().inject(this)

        val liveData = MutableLiveData<String>()
        liveData.observe(viewLifecycleOwner) { result ->
            textView3?.text = result
        }

        vm.getResult("ott")
            .subscribe { liveData.value = it }
    }

    private fun <T : Any> LiveData<T>.observe(
        lifecycleOwner: LifecycleOwner,
        observer: (T) -> Unit
    ) {
        observe(lifecycleOwner, Observer<T> { t -> observer(t) })
    }
}
